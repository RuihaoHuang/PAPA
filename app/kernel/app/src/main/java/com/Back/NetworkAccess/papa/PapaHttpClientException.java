package com.Back.NetworkAccess.papa;

public abstract class PapaHttpClientException extends Exception
{
    public PapaHttpClientException()
    {

    }

    public PapaHttpClientException(String s)
    {
        super(s);
    }
}
