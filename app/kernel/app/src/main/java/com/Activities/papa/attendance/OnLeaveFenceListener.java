package com.Activities.papa.attendance;

public interface OnLeaveFenceListener {
    void onLeaveFence(GeoFence.Fence fence);
}
