package com.Activities.papa.attendance;

/**
 * When a service signs in successfully, this function is called.
 */
public interface OnSignInSuccessListener {
    void onSignInSuccess();
}
