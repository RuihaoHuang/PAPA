package com.Activities.papa.attendance;

import com.Settings.Settings;

/**
 * Created by alexwang on 11/19/15.
 */
public interface OnSignOutSuccessListener {
    void onSignOutSuccess(Settings.Lesson lesson);
}
