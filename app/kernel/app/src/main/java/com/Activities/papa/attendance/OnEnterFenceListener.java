package com.Activities.papa.attendance;

public interface OnEnterFenceListener {
    void onEnterFence(GeoFence.Fence fence);
}
